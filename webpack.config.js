const path = require("path");
const webpack = require("webpack");
const HTMLWebpackPlugin = require("html-webpack-plugin");

module.exports = {
  entry: "./src/renderer/app.tsx",
  devtool: 'inline-source-map',
  mode: "development",
  // output: {
  //   filename: "bundle.js",
  //   path: path.resolve(__dirname, 'dist'),
  //   publicPath: "/",
  // },
  module: {
    rules:[
      {
        test: /\.(ts|tsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env', '@babel/react'],
          }
        }
      },
      {
        test: /\.html$/,
        use: "html-loader"
      },
      /*Choose only one of the following two: if you're using 
      plain CSS, use the first one, and if you're using a
      preprocessor, in this case SASS, use the second one*/
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"],
      },
      {
        test: /\.scss$/,
        use:[
          "style-loader",
          "css-loader",
          "sass-loader"
        ],
      },
    ], 

  },  
  resolve: {
    extensions: ['.tsx', '.ts', '.js'],
    },
  plugins: [
    new HTMLWebpackPlugin({
      template: "./src/renderer/index.html"
    }),
  ],
devServer: {
  historyApiFallback: true,
  port: 9000,
},
}