import React from 'react';
import ReactDOM,  { hydrate, render } from 'react-dom';
import loadable,{ loadableReady } from '@loadable/component'

const OtherComponent = loadable(() => import('./test'))
function App() {
  return (
    <OtherComponent />
  )
   
}

loadableReady(() => {
  ReactDOM.render(<App />, document.getElementById('root'));
});
