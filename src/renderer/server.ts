const express = require('express');
const dotenv = require('dotenv');

dotenv.config();

const app = express();

app.use('/', express.static(`${__dirname}/../../dist`));
app.use('/*', express.static(`${__dirname}/../../dist`));

const { PORT } = process.env;

const server = app.listen(PORT, () => {
  const host = server.address().address;
  const { port } = server.address();

  console.log(`🚀 Listening at http://${host}:${port}`);
});
